# RPM Fusion
# Source -> https://rpmfusion.org/Configuration
echo 
echo "Adding RPM Fusion Repository"
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
echo "Done"
echo 



# Alacritty
# Source -> https://github.com/alacritty/alacritty/blob/master/INSTALL.md
echo
echo "Enabling COPR repo for Alacritty Terminal"
sudo dnf copr -y enable pschyska/alacritty
echo "Done"
echo



# i3-gaps
# Source -> https://copr.fedorainfracloud.org/coprs/fuhrmann/i3-gaps/
echo
echo "Enabling COPR repo for i3-gaps"
sudo dnf copr enable fuhrmann/i3-gaps
echo "Done"
echo



# i3-blocks
# Source -> https://copr.fedorainfracloud.org/coprs/skidnik/i3blocks/
echo
echo "Enabling COPR repo for i3-blocks"
sudo dnf copr -y enable skidnik/i3blocks
echo "Done"
echo


# Brave browser repo
# Source -> https://brave.com/linux/#linux
echo
echo "Adding Brave Browser repo"
sudo dnf install -y dnf-plugins-core
sudo dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/
sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
echo "Done"
echo



# Sublime Text repo
# Source -> https://www.sublimetext.com/docs/3/linux_repositories.html#dnf
echo
echo "Adding Sublime Text repo"
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo
echo "Done"
echo



# VSCode repo
# Source -> https://code.visualstudio.com/docs/setup/linux#_rhel-fedora-and-centos-based-distributions
echo
echo "Adding VSCode repo"
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
echo "Done"
echo

