PKGS=(

    # Terminals
    alacritty
    rxvt-unicode
    
    # i3 packages
    i3-gaps
    rofi
    dmenu
    picom
    i3blocks
    i3lock
    i3status
    feh
    light

    lxappearance

    # Ranger-fm and Ueberzug dependencies
    python-devel
    libX11-devel
    libXext-devel

)

echo
echo "Starting to install i3-packages from repos"
echo

for PKG in "${PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    sudo dnf install -y "$PKG"
done

echo
echo "Done installing core i3 packages!!"
echo






pipPKGS=(
    
    pywal               # Pywal (Color-scheme generator) -> https://github.com/dylanaraps/pywal
    ranger-fm           # Ranger (terminal file-manager) -> https://github.com/ranger/ranger
    ueberzug            # Ueberzug (image viewer for ranger-fm and more) -> https://github.com/seebye/ueberzug

)


echo
echo "Installing python packages"
echo

for PKG in "${pipPKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    sudo pip install "$PKG"
done

echo
echo "Done installing python packages!!"
echo