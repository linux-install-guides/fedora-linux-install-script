echo
echo "INSTALLING SOFTWARE"
echo


PKGS=(

    # DNF stuff
    kernel-devel

    # Fonts
    mozilla-fira-mono-fonts
    fontawesome-fonts-web
    fontawesome-fonts
    fira-code-fonts

    # Themes
    la-capitaine-icon-theme 
    numix-icon-theme-circle 
    mono-icon-theme 
    flat-remix-icon-theme 
    faience-icon-theme
    la-capitaine-cursor-theme
    arc-theme
    numix-gtk-theme 
    flat-remix-gtk3-theme
    elementary-theme
    elementary-icon-theme
    slick-greeter

    # Utilities
    htop
    neofetch
    ytop
    vim
    git
    zsh
    lolcat
    elementary-planner

    # Media
    vlc
    gthumb


    # NVIDIA packages
    akmod-nvidia
    xorg-x11-drv-nvidia-cuda

    # Browser
    chromium-browser-privacy
    brave-browser

    # Editors
    code
    sublime-text

    # XFCE Desktop Env
    # @xfce-desktop

)


for PKG in "${PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    sudo dnf install -y "$PKG"
done

echo
echo "Done!"
echo




# Source -> https://docs.fedoraproject.org/en-US/quick-docs/assembly_installing-plugins-for-playing-movies-and-music/

echo
echo "Installing Multimedia packages"
echo


sudo dnf install gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel

sudo dnf install lame\* --exclude=lame-devel

sudo dnf group upgrade --with-optional Multimedia


echo
echo "Done!"
echo

